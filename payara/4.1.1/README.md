# Supported tags and respective ```Dockerfile``` links
* 4, 4.1, 4.1.1, latest [payara/dockerfile](https://bitbucket.org/bongladesch/docker/src/df13772f04e65c7fc0c86100fc91646a18c27cb9/payara/4.1.1/dockerfile?at=master&fileviewer=file-view-default)

# Description
Payara is a full Java-EE server based on Glasfish wich is the reference implementation of Java-EE. This image is based on [bongladesch/openjdk:8](https://hub.docker.com/r/bongladesch/openjdk/) image. Payara is pre configured for development and production usage and for creating a JDBC-Connection-Pool with PostgreSQL or MySQL database. For more configuration add Shell-Scripts to the ```/entrypoint``` directory. These scripts will be executed in alphabetic order. For using asadmin tool the password file is located under ```/usr/share/payara41/bin/pwdfile```. After running all configuration scripts the password file will be deleted. For using another DBMS add the appropriated JDBC-Driver to ```/usr/share/payara41/glassfish/lib/```, set the ** USE_DBCP ** to false and create the connection pool with your own configuration script in the ```/entrypoint``` directory.

# Usage
Run Payara server without database connection pool:
```
docker run -it --name payara -p 8080:8080 -p 4848:4848 -p 9009:9009 bongladesch/payara
```
For a more detailed configuration use one of the [docker-compose files](https://bitbucket.org/bongladesch/docker/src/df13772f04e65c7fc0c86100fc91646a18c27cb9/payara/4.1.1/compose/?at=master) directly or as template for your own configuration.

For deploying with autodeploy function of glasfish use the following command to copy your builded webapplication into to container. You can run this command multiple times. The new file overrides the old one. ** SERVER_MODE ** has to be "development".
```
docker cp <webapp.war> <container-name>:/usr/share/payara41/glassfish/domains/payaradomain/autodeploy
```

For production usage create a new image based on this image and create a new directory for your webapplication. After copying your webapplication into the image put following command in a script in the ```/entrypoint``` directory to deploy your application production ready. The script will be executed automatically.
```
asadmin -u ${ADMIN_USER} --passwordfile /usr/share/payara41/bin/pwdfile --interactive=false deploy /path/to/webapp.war
```

# Environment Variables

* ** ADMIN_USER **
User name of the admin user. Default is "admin".

* ** ADMIN_PASSWORD **
Admin password for user "admin". Default is "password".

* ** SERVER_MODE **
Set server mode to "production" if you want to use this image to deploy applications in production environment. Default is "development". In development mode autodeploy, and a finer logging is enabled.

** JVM Options **
* ** XMX **
The flag Xmx specifies the maximum memory allocation pool for a Java Virtual Machine (JVM). Default is "2048".

* ** XMS **
The flag Xms specifies the initial memory allocation pool. Default is "2048".

* ** XMN **
The flag Xmn specifies the size of the heap for the young generation. Default is "1024".

** Variables for Database Connection Pool **
* ** USE_DBCP **
Set to "true" for creating a database connection pool. Default is "false".

* ** DB_SYSTEM **
Image is pre configured to use PostgreSQL and MySQL. The default value is "postgresql". If you want to use a MySQL system set this variable to "mysql". Set USE_DBCP to "true" is requiered.

* ** DB_DELAY **
Payara sleeps for the value of this variable in seconds if the DBMS needs time to set up. Default ist 0. If DB_SYSTEM is set to "mysql" set this variable to "30" is recommended.

* ** DB_USER **
User of the database. Default is "payara".

* ** DB_PASSWORD **
Password for database user. Default is "password".

* ** DB_HOST **
IP-Address/Hostname of DBMS. Default is "localhost".

* ** DB_PORT **
Database port. Default is "5432" (PostgreSQL default port).

* ** DB_NAME **
Name of the database. Default is "payara".

# Details
Payara Server installation is located in the ```/usr/share/payara41``` directory. This directory is the default working directory of the docker image.
