######################################################
#
# Start domain via asadmin, change
# admin-password, enable remote admin console.
# Optional set JDBC Connection-Pool and
# external OpenMQ Connection.
# After restarting the domain all scripts in
# entrypoint directory will be executedin and
# finaly sleep in endless loop to keep service alive.
#
######################################################
#!/bin/bash
set -e

# start domain
asadmin start-domain --port 8080 --debug=false payaradomain

# create password files
echo "AS_ADMIN_PASSWORD=" > tmpfile
echo "AS_ADMIN_NEWPASSWORD="${ADMIN_PASSWORD} >> tmpfile
echo "AS_ADMIN_PASSWORD="${ADMIN_PASSWORD} > pwdfile

# delete default JVM heap space
asadmin -u ${ADMIN_USER} \
        --passwordfile tmpfile \
        --interactive=false \
        delete-jvm-options "-Xmx2g:-Xms2g"

# set JVM options for heap space and file encoding
asadmin -u ${ADMIN_USER} \
        --passwordfile tmpfile \
        --interactive=false \
        create-jvm-options \
        "-Xmx${XMX}m:-Xms${XMS}m:-Xmn${XMN}m:-Dfile.encoding=UTF8:-XX\:MaxPermSize=512m:-Dfish.payara.classloading.delegate=false:-XX\:+DisableExplicitGC"

# change admin password
asadmin -u ${ADMIN_USER} \
        --passwordfile tmpfile \
        --interactive=false \
        change-admin-password

# enable admin console
asadmin -u ${ADMIN_USER} \
        --passwordfile pwdfile \
        --interactive=false \
        enable-secure-admin

# Development-Mode
if [ "${SERVER_MODE}" = "development" ]
then
    asadmin -u ${ADMIN_USER} \
            --passwordfile pwdfile \
            --interactive=false \
            set-log-levels java.util.logging.ConsoleHandler=FINEST
    
    asadmin -u ${ADMIN_USER} \
            --passwordfile pwdfile \
            --interactive=false \
            set server.admin-service.das-config.autodeploy-enabled=true
else
    asadmin -u ${ADMIN_USER} \
            --passwordfile pwdfile \
            --interactive=false \
            set server.admin-service.das-config.autodeploy-enabled=false
    
    asadmin -u ${ADMIN_USER} \
            --passwordfile pwdfile \
            --interactive=false \
            set server.admin-service.das-config.dynamic-reload-enabled=false
fi

# Set IP-Adress of container
ip_address=$(awk 'END{print $1}' /etc/hosts)

# http listener
asadmin -u ${ADMIN_USER} \
        --passwordfile pwdfile \
        --interactive=false \
        set server-config.network-config.network-listeners.network-listener.http-listener-1.address=${ip_address}

# https listener
asadmin -u ${ADMIN_USER} \
        --passwordfile pwdfile \
        --interactive=false \
        set server-config.network-config.network-listeners.network-listener.http-listener-2.address=${ip_address}

# admin console listener
asadmin -u ${ADMIN_USER} \
        --passwordfile pwdfile \
        --interactive=false \
        set server-config.network-config.network-listeners.network-listener.admin-listener.address=${ip_address}

# orb listener
asadmin -u ${ADMIN_USER} \
        --passwordfile pwdfile \
        --interactive=false \
        set server.iiop-service.iiop-listener.orb-listener-1.address=${ip_address}

# jmx listener
asadmin -u ${ADMIN_USER} \
        --passwordfile pwdfile \
        --interactive=false \
        set server-config.admin-service.jmx-connector.system.address=${ip_address}

# Set Threadpool size
asadmin -u ${ADMIN_USER} \
        --passwordfile pwdfile \
        --interactive=false \
        set server.thread-pools.thread-pool.http-thread-pool.max-thread-pool-size=200
asadmin -u ${ADMIN_USER} \
        --passwordfile pwdfile \
        --interactive=false \
        set server.thread-pools.thread-pool.http-thread-pool.min-thread-pool-size=200

# JDBC
sleep ${DB_DELAY}s
if [ "${USE_DBCP}" = "true" ]
then
    if [ "${DB_SYSTEM}" = "postgresql" ]
    then
        asadmin -u ${ADMIN_USER} \
                --passwordfile pwdfile \
                --interactive=false create-jdbc-connection-pool \
                --datasourceclassname org.postgresql.ds.PGConnectionPoolDataSource \
                --restype javax.sql.ConnectionPoolDataSource \
                --ping true \
                --property portNumber=${DB_PORT}:password=${DB_PASSWORD}:user=${DB_USER}:serverName=${DB_HOST}:databaseName=${DB_NAME} \
                ${CONNECTION_POOL_NAME}
    elif [ "${DB_SYSTEM}" = "mysql" ]
    then
        asadmin -u ${ADMIN_USER} \
                --passwordfile pwdfile \
                --interactive=false create-jdbc-connection-pool \
                --datasourceclassname com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource \
                --restype javax.sql.DataSource \
                --ping true \
                --property portNumber=${DB_PORT}:password=${DB_PASSWORD}:user=${DB_USER}:serverName=${DB_HOST}:databaseName=${DB_NAME} \
                ${CONNECTION_POOL_NAME}
    fi                         
    
    asadmin -u ${ADMIN_USER} \
            --passwordfile pwdfile \
            --interactive=false create-jdbc-resource \
            --connectionpoolid ${CONNECTION_POOL_NAME} \
            ${CONNECTION_RESOURCE_NAME}
fi

# execute all executable scripts in entrypoint directory
files=/entrypoint/*.sh
for file in $files
do
    if [ -x ${file} ]
    then
        ./${file}
    fi
done

# Restart domain
if [ "${SERVER_MODE}" = "development" ]
then
    asadmin restart-domain --port 8080 --debug=true payaradomain
elif [ "${SERVER_MODE}" = "production" ]
then
    asadmin restart-domain --port 8080 --debug=false payaradomain
fi

# Remove password files after running all scripts
cd ${PAYARA_LOCATION}/bin
rm pwdfile tmpfile

echo "Service is running now ..." 

# Sleep in endless loop to keep service alive
while true
do
    sleep 1d
done
